var articleApp=angular.module('articleApp', ['ngResource']);

articleApp.filter("sanitize", ['$sce', function($sce) {
      //var div = document.createElement('div');
      return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
      }
   }]);

articleApp.directive('navigateBar', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: 'templates/nav-bar.html',
  };
});

articleApp.directive('generateArticles', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: 'templates/article-list.html',
      scope: {
        articles: '=',
        articleType: '@',
        styling: '@'
      },
      link: function(scope, element, attrs) {
          scope.showArticles = function(articleType, isFeature) {
            //decide which type of article to show
            if (articleType == 'highlight' && isFeature == true) {
              return true;
            } else if (articleType == 'regular' && isFeature != true) {
              return true;
            } else {
              return false;
            };

          };
      },
  };
});

articleApp.directive('classNotes', function () {
  return {
      restrict: 'AE',
      replace: 'true',
      transclude: 'element',
      templateUrl: 'templates/class-notes.html',
      scope: {
        notes: '=',
      },
  };
});

articleApp.directive('addFooter', function() {
  return {
      restrict: 'AE',
      replace: 'true',
      templateUrl: 'templates/footer.html',
  };
});



articleApp.controller('issueCtrl', function($scope) {
  $scope.issues = [
      {
        "self":"http:\/\/westerntodaydev.wwu.edu\/node?type=issue",
        "first":"http:\/\/westerntodaydev.wwu.edu\/node?type=issue\u0026page=0",
        "last":"http:\/\/westerntodaydev.wwu.edu\/node?type=issue\u0026page=0",
        "list":[
            {
                "field_edition":"Winter\/Spring 2016",
                "field_issue_front_layout":"home_layout_a",
                "field_edition_status":"proposed_edition",
                "field_publication":"Window",
                "nid":"110019",
                "vid":"111719",
                "is_new":false,
                "type":"issue",
                "title":"Window-Winter\/Spring 2016",
                "language":"und",
                "url":"http:\/\/westerntodaydev.wwu.edu\/publication\/window-winterspring-2016",
                "edit_url":"http:\/\/westerntodaydev.wwu.edu\/node\/110019\/edit",
                "status":"1",
                "promote":"0",
                "sticky":"0",
                "created":"1448044886",
                "changed":"1448045409",
                "author":{
                    "uri":"http:\/\/westerntodaydev.wwu.edu\/user\/296",
                    "id":"296",
                    "resource":"user"
                },
                "log":"",
                "revision":null,
                "body":[
                ],
                "views":"9",
                "day_views":"0",
                "last_view":"1448047394"
            }
        ]
    }
  ]
});

articleApp.controller('coverCtrl', function($scope) {
  $scope.covers = [
    {
      'url': '3.html',
      'imageurl': 'images/cover03.jpg',
      'title': 'From Here to ODESZA',
      'field_byline': 'Two grads are huge in electronic dance music'
    },
    {
      'url': '2.html',
      'title': 'The Truth is Out There',
      'field_byline': 'Western’s Melissa Rice is part of a NASA team uncovering the secrets of Mars',
      'imageurl': 'images/cover02.jpg'
    },
    {
      'url' : '1.html',
      'title': 'This Brain is Fighting Back',
      'field_byline': 'Jeff Carroll spends his days studying the monster destined to kill him',
      'imageurl': 'images/cover01.jpg'
    }
  ];
});

articleApp.controller('articleCtrl', function($scope) {
  $scope.articles = [
    {
      "body":{
          "value":"\u003Cp\u003EU.S. Marine veteran Chris Brown (\u0026rsquo;12, Human Services) says his burden from three deployments to Iraq and Afghanistan is like a bag of rocks. The rocks represent the injuries, physical and mental, he and his fellow Marines suffered, and the guilt that he made it back home when 41 of his friends never did.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; He carries this symbolic burden over his shoulder wherever he goes and, sometimes, he just wants to set it down.\u003C\/p\u003E\n\n\u003Cp\u003E\u003Cimg alt=\u0022Planting seeds\u0022 style=\u0022float:right; height:524px; width:700px\u0022 class=\u0022file-default media-element\u0022 typeof=\u0022foaf:Image\u0022 src=\u0022http:\/\/westerntodaydev.wwu.edu\/sites\/westerntodaydev.wwu.edu\/files\/image002.jpg\u0022 width=\u0022700\u0022 height=\u0022524\u0022 \/\u003E\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; \u0026ldquo;The tough part,\u0026rdquo; he writes, \u0026ldquo;I\u0026rsquo;m afraid to lose those rocks. I\u0026rsquo;m afraid to lose them because of what they have done to shape my character and who I am as a man.\u0026rdquo;\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; But by sharing his experiences, Brown feels as though he can pass on some of those rocks for safekeeping, easing his burden.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; He\u0026rsquo;s also taken a few of those rocks and planted them in the earth. What\u0026rsquo;s growing there now will help many other veterans ease their own burdens.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; Brown is founder and director of Growing Veterans, an organization combining sustainable farming with veteran reintegration.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; The concept grew out of his experiences while studying Human Services at Western and working in Western\u0026rsquo;s Veteran\u0026rsquo;s Services office through the VetCorps program. He and some of the \u0026ldquo;regulars\u0026rdquo; from the veterans office, many of them students at Huxley College of the Environment, often discussed agriculture and food systems.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; Brown saw that sustainable agriculture practices could use support, as could veterans transitioning back into civilian life. Together, the two create some very powerful outcomes, he thought.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; Growing Veterans was also a way to support himself as he works through his own struggles as a combat veteran. Studying Human Services gave him a way to channel his energy.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; \u0026ldquo;I wanted to focus my learning on veterans\u0026rsquo; issues in an attempt to both understand myself better but also to enter a career centered around helping veterans,\u0026rdquo; Brown says.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; More personally, Brown says, his work is \u0026ldquo;an attempt to justify the guilt that I carried about the guys who never made it home.\u0026rdquo;\u003Cbr \/\u003E\n\t\u0026nbsp; NEW GROWTH\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; On a sunny, crisp March morning, Brown gives a tour of the Growing Veterans farm in rural Whatcom County. Beds of kale and leeks have wintered over, and a new field on the southern end of the property is freshly tilled. One greenhouse is already filled with starts for the planting season, and the other greenhouse is nearly ready for tomatoes, basil and peppers to be sold at farmer\u0026rsquo;s markets outside Veterans Affairs hospitals in the Seattle area.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; On this day, a handful of people are working on the farm. Outreach Coordinator and Market Manager Matt Aamot, an Army veteran of Kuwait and Bosnia, is showing around new employee Dan Robinson. It is Robinson\u0026rsquo;s first day as the field manager, recruited from a veteran-farming program in Colorado. Meanwhile, Billy Pacaccio is working on the junction box in the barn. Pacaccio was a Navy veteran and a Whatcom Community College student when he joined Growing Veterans in its second year.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; He says his job is not just helping to manage the farm, but building a community in which his fellow veterans, with all kinds of backgrounds and experiences, come together around a goal.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; \u0026ldquo;They found the earth in common,\u0026rdquo; Pacaccio says, \u0026ldquo;and from there they prospered from it.\u0026rdquo;\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; Growing Veterans has five full-time paid staff members and four part-time employees, all but one a veteran. Three more veterans also work for Growing Veterans through AmeriCorps.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; In addition to paid staff, volunteers \u0026ndash; 370 last year alone \u0026ndash; help in the field or greenhouse, or sell produce at farmers markets. Others help with peer support or community outreach or with tasks such as grant writing and advising.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; Brown, 28, grew up King County and didn\u0026rsquo;t have a background in farming, so when he decided to start a veterans\u0026rsquo; farming group he first needed some basic training. He turned to Growing Washington, an Everson-based nonprofit supporting the growth of sustainable agriculture. Brown was hired on as a farm hand, working 20 hours a week to learn all the facets of farming, from planting to delivery.\u003C\/p\u003E\n\n\u003Cp\u003E\u0026nbsp; He spent another 20 volunteer hours a week developing the Growing Veterans concept. By the time the 2013 growing season rolled around, Growing Washington had helped Brown and Growing Veterans secure a three-acre farm in Whatcom County that once belonged to the Bellingham Food Bank.\u003C\/p\u003E\n",
          "summary":"",
          "format":"clean_html"
      },
      "field_byline":"One alum brings fellow combat vets to the farm to grow vegetables and help each other heal.",
      "field_article_author":"Hilary Parker",
      "field_pull_quotes":[
          "Brown saw that sustainable agriculture practices could use support, as could veterans transitioning back into civilian life. ",
          "\u201cThey found the earth in common,\u201d Pacaccio says, \u201cand from there they prospered from it.\u201d ",
          "\u201cThe reason I can confidently say it is innovative is because it\u2019s not being designed by doctors; it\u2019s being designed by the veterans who are involved,\u201d",
          "Here\'s another pullquote for you",
          //"I'll quote whatever I whenever I want.",
          //"We can quote if we want to",
          //"You can't handle all this quotey quotey goodness.",
          //"These quotes are getting a little out of hand. Maybe pull it back a bit, you know, edit."
      ],
      "field_slideshow_images":[
          {
              "file":{
                  "uri":"article_001/image001.jpg",
                  "id":"24250",
                  "resource":"file"
              }
          },
          {
              "file":{
                  "uri":"article_001/image002.jpg",
                  "id":"24251",
                  "resource":"file"
              }
          },
          {
              "file":{
                  "uri":"article_001/image003.jpg",
                  "id":"24252",
                  "resource":"file"
              }
          },
          {
              "file":{
                  "uri":"article_001/image004.jpg",
                  "id":"24253",
                  "resource":"file"
              }
          }
      ],
      "field_other_images":[
      ],
      "field_featured_image":{
          "file":{
              "uri":"images/article001.jpg",
              "id":"24246",
              "resource":"file"
          }
      },
      "field_feature":true,
      "nid":"110020",
      "vid":"111720",
      "is_new":false,
      "type":"publication_story",
      "title":"Resilience Grows Here",
      "language":"und",
      "url":"long-article-layout_01.html",
      "edit_url":"http:\/\/westerntodaydev.wwu.edu\/node\/110020\/edit",
      "status":"0",
      "promote":"0",
      "sticky":"0",
      "created":"1448048264",
      "changed":"1450198121",
      "author":{
          "uri":"http:\/\/westerntodaydev.wwu.edu\/user\/296",
          "id":"296",
          "resource":"user"
      },
      "log":"",
      "revision":null,
      "views":"18",
      "day_views":"13",
      "last_view":"1450198122"

    },
    {
      "url":"",
      "field_featured_image":{
          "file":{
              "uri":"images/article002.jpg",
              "id":"24246",
              "resource":"file"
          }
      },
      "title":"An Ocean of Stories",
      "field_byline":"How Western\"s vast Mongolia Collection became one of the largest in the country ",
      "field_feature":true,
    },
    {
      "url":"",
      "field_featured_image":{
          "file":{
              "uri":"images/article003.jpg",
              "id":"24246",
              "resource":"file"
          }
      },
      "title":"Service Legacy",
      "field_byline":"Two alumni and Sociology faculty study the long-term effects of military service ",
      "field_feature":true,
    },
    {
      "url": "short-article-no-slides.html",
      "field_featured_image":{
          "file":{
              "uri":"images/article004.jpg",
              "id":"24246",
              "resource":"file"
          }
      },
      "title": "A Look Back: A 1947 Chemistry Lab",
      "field_byline": "Students Ann Pearson and Keith Booman work in a Chemistry lab in Old Main in this 1947 photo.",
    },
    {
      "body":{
          "value":"<p>At the height of the Great Depression, Alice Cowgill (’31, Teaching Certificate) graduated from Washington State Normal School and went to work at a school in Ferndale, teaching grades one through 12 in a single classroom, until she married the love of her life, Gordon “Bus” Fraser.</p><p>Fast-forward to 2015, and the Frasers’ legacy at Western is set to reach thousands of students. The Frasers left gifts to Western totaling more than $8 million, the largest gift in Western’s history, which will support scholarships for public high school students from Whatcom, Skagit, Island and Snohomish counties. Several academic areas, such as music, biology and the College of Business and Economics will also benefit. And a new Fraser Lectureship will bring internationally known scholars to the university.</p><p>The Frasers first decided to support Western in 1986 – Fraser Hall was named in their honor in 1995. Bus Fraser owned a successful Chevrolet dealership in Bellingham and passed away in 2004. Alice died in 2014 at age 102 – soon after her 100th birthday, she had cut the ribbon at a rebuilt Whatcom Middle School as the school’s “oldest living graduate.”</p><p>“They were both always interested in participating in and giving back to the community,” says Al Froderberg, a longtime Western administrator. “Both Bus and Alice wanted to give money to Western because this is where they made their money, in this community. You leave it where you made it.”</p>",
          "summary":"",
          "format":"clean_html"
      },
      "url": "short-article-slideshow.html",
      "field_featured_image":{
          "file":{
              "uri":"images/article005.jpg",
              "id":"24246",
              "resource":"file"
          }
      },
      "field_slideshow_images":[
          {
              "file":{
                  "uri":"article_002/image002.jpg",
                  "id":"24250",
                  "resource":"file"
              }
          },
          {
              "file":{
                  "uri":"article_002/image003.jpg",
                  "id":"24251",
                  "resource":"file"
              }
          },
      ],

      "title": "Bellingham\'s Frasers gave millions for scholarships, academic programs",
      "field_byline": "Couple began supporting Western in 1986 -- Fraser Hall is named in their honor"
    },
    {
      "field_featured_image":{
          "file":{
              "uri":"images/article006.jpg",
              "id":"24246",
              "resource":"file"
          }
      },      "title": "He met WWU students in Rwanda and now studies English at Western",
      "field_byline": "Hassan Buymvuhore is enrolled in Western\"s Intensive English Program"
    },
    {
      "field_featured_image":{
          "file":{
              "uri":"images/article007.jpg",
              "id":"24246",
              "resource":"file"
          }
      },      "title": "A Viking in the Japanese House of Representatives",
      "field_byline": "Hokkaido lawmaker soaked up art, music, scenery in Bellingham while earning an MBA at Western"
    },
    {
      "field_featured_image":{
          "file":{
              "uri":"images/article008.jpg",
              "id":"24246",
              "resource":"file"
          }
      },      "title": "Meet a new molecule",
      "field_byline": "Trilobite molecules resemble marine fossils"
    },
    {
      "field_featured_image":{
          "file":{
              "uri":"images/article009.jpg",
              "id":"24246",
              "resource":"file"
          }
      },      "title": "Ice Cream Genius",
      "field_byline": "Is there anything Tyler Malek can\"t put in a frozen scoop?"
    },
    {
      "field_featured_image":{
          "file":{
              "uri":"images/article010.jpg",
              "id":"24246",
              "resource":"file"
          }
      },      "title": "An ancient Skagit Valley tree becomes a work of art",
      "field_byline": "The tree had fallen during the age of the Roman Empire"
    },
    {
      "field_featured_image":{
          "file":{
              "uri":"images/article011.jpg",
              "id":"24246",
              "resource":"file"
          }
      },      "title": "Students’ solar window wows the EPA",
      "field_byline": "Window harnesses sun\"s energy to automatically open and close"
    },
    {
      "field_featured_image":{
          "file":{
              "uri":"images/article012.jpg",
              "id":"24246",
              "resource":"file"
          }
      },      "title": "Sehome Hill Arboretum",
      "field_byline": "For decades, students have gotten their nature fix while meandering the trails of the wooded hillside next to Western\"s campus"
    }
    ];
});

articleApp.controller('notesCtrl', function($scope) {
    $scope.notes = [
      {
      "field_graduation_year":"2014",
      "field_photograph":{
          "file":{
              "uri":"images/article012.jpg",
              "id":"24254",
              "resource":"file"
          }
      },
      "field_class_note_body":"Elena Bary (Environmental Science \u2013 Toxicology) joined AmeriCorps National Civilian Community Corps and works with Blue Water Baltimore, a nonprofit organization.",
      "nid":"110027",
      "vid":"111736",
      "is_new":false,
      "type":"class_note",
      "title":"Elana Bary",
      "language":"und",
      "url":"http:\/\/westerntodaydev.wwu.edu\/content\/elana-bary",
      "edit_url":"http:\/\/westerntodaydev.wwu.edu\/node\/110027\/edit",
      "status":"0",
      "promote":"0",
      "sticky":"0",
      "created":"1450217665",
      "changed":"1450217665",
      "author":{
          "uri":"http:\/\/westerntodaydev.wwu.edu\/user\/296",
          "id":"296",
          "resource":"user"
      },
      "log":"",
      "revision":null,
      "body":[
      ],
      "views":"2",
      "day_views":"2",
      "last_view":"1450217666"
      },
      {
      'field_graduation_year': '1998',
      'title': 'Kelly Liske',
      'field_class_note_body': 'Kelly Liske (Accounting), executive vice president and chief banking officer for First Federal Bank in Port Townsend, recently became a board member for the Jefferson Healthcare Foundation.',
      'noteImg': ''
      },
      {
      'field_graduation_year': '1960',
      'title': 'Bill Wright',
      'field_class_note_body': 'Bill Wright (Elementary Education) was recently inducted into the Pacific Northwest Golf Association Hall of Fame. A former professional golfer and the first African-American golfer to win a USGA national championship, Wright continues to teach golf in Los Angeles.',
      'noteImg': ''
      },

    ];
});
