$(document).ready(function(){
  $(window).scroll(function() {

    //change menu position to above or below nav bar depending on position
    if($(window).width() > 1550) {
      $(".front-menu").css("bottom","0");
      $(".front-menu").css("top","50%");
    } else if($(this).scrollTop() > 400) {
      $(".front-menu").css("bottom","inherit");
      $(".front-menu").css("top","70px");
    }
    else {
      $(".front-menu").css("top","inherit");
      $(".front-menu").css("bottom","70px");
    }

    //fade logo in on botton nave menu
    if($(this).scrollTop() > 400) {
      $(".front-logo").fadeIn("slow");
    }
    else {
      $(".front-logo").fadeOut("slow");
    }

    //make menu sticky on front page
    if($(this).scrollTop() >= $(window).height() - 70) {
      $(".front-nav").addClass("sticky");
    }
    else {
      $(".front-nav").removeClass("sticky");
    }

  });

  //cover slideshow
  $('.cover-slideshow').slick({
    prevArrow: ".left-arrow",
    nextArrow: ".right-arrow",
    adaptiveHeight: false,
    accessibility: true,

    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',

    autoplay: true,
    autoplaySpeed: 5000,

  });

  //class notes slideshow
  $('.note-scroller').slick({
    prevArrow: ".left-arrow",
    nextArrow: ".right-arrow",
    adaptiveHeight: true,
    accessibility: true,

  });

  //article slideshows
  $('.slides').slick({
    prevArrow: ".left-arrow",
    nextArrow: ".right-arrow",
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true
  });

  //push pullquotes into paragraphs on article pages
  var pullquotes = document.getElementsByTagName("blockquote");
  var paragraphs = document.getElementsByTagName("p");
  var spacer = Math.floor(paragraphs.length / (pullquotes.length + 1));
  var fromtop = spacer;

  for (i = 0; i < pullquotes.length; i++ ) {
    if (i % 3 == 0) {
      currentAlignment = "center";
    } else if (i % 2 == 0) {
      currentAlignment = "left";
    } else {
      currentAlignment = "right";
    }

    $("p:nth-of-type(" + fromtop + ")").after(pullquotes[i]);

    $(pullquotes[i]).addClass(currentAlignment);

    fromtop = fromtop + spacer;
  }


  //push slider up into paragraphs on article pages
  var halfDown = Math.floor(paragraphs.length/2);
  var slideshow = document.getElementsByClassName("push-up");

  $("p:nth-of-type(" + halfDown + ")").after(slideshow);


});
